package org.diegokiyoshi;

import java.util.ArrayList;
import java.util.List;

import org.diogobarbosa.Main;
import org.diogobarbosa.Pessoa;

public class TreinamentoForEach {
	public static void main(String[] args) {
		
		List<Pessoa> pessoa = new ArrayList<>();
		Main main = new Main();
		pessoa = main.obterListaPessoas();
		
		for (Pessoa pessoa2 : pessoa) {
			System.out.println(pessoa2.getNome()); 
		}
		
		if(pessoa.get(1).equals(pessoa.get(0))) {
			System.out.println("� a mesma pessoa");
		} else {
			System.out.println("N�o � a mesma pessoa");
		}
		
	}
}
