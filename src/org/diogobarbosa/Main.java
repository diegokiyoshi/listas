package org.diogobarbosa;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

	// O OBJETIVO DESSE EXERCICIO � SOMENTE MUDAR A PROFISS�O DE PEDRO
	// ESCREVENDO C�DIGO SOMENTE NOS LUGARES PERMITIDOS ENTRE OS BLOCOS COMENTADOS.

	
	public static void main(String[] args) {

		// TRECHO ONDE VOC� PODE ESCREVER C�DIGO, SE QUISER. - INICIO
		//
		Main main = new Main();
		List<Pessoa> listaPessoa = main.obterListaPessoas();
		
		Pessoa pedro = null;
		int index = 0 ;
		
		
		
		//*****

		for (int i = 0; i < listaPessoa.size(); i++) {
			if(listaPessoa.get(i).getNome().equalsIgnoreCase("pedro")) {
				pedro = listaPessoa.get(i);
				index = i;
			}	
		}
		//*****

		
		
		
		//*****

		for(Pessoa p : listaPessoa) {
			if(p.getNome().equalsIgnoreCase("pedro")) {
				pedro = p;
			}
		}
		//*****

		
		
		
		//*****
		Pessoa pedroAProcurar = new Pessoa();
		pedroAProcurar.setNome("pedro");
		
		Integer indexDoPedro = listaPessoa.indexOf(pedroAProcurar);
		pedro = listaPessoa.get(indexDoPedro);
		//*****

		
		
		//
		// TRECHO ONDE VOC� PODE ESCREVER C�DIGO, SE QUISER. - FIM
		
				Scanner scanner = new Scanner(System.in);
				System.out.println("Digite a nova profiss�o do Pedro: \n");
				String novaProfissaoDoPedro = scanner.nextLine();
		
		// ATUALIZE A PROFISSAO DO PEDRO
		// ATEN��O: NESSE TRECHO EM ESPEC�FICO, VOC� EST� PROIBIDO DE USAR 'FOR' ou
		// 'while'
		// TRECHO ONDE VOC� PODE ESCREVER C�DIGO, SE QUISER. - INICIO
		
				//POSSIBILDIADES DE PEGAR PEDRO
				pedro.setProfissao(novaProfissaoDoPedro);
				listaPessoa.get(index).setProfissao(novaProfissaoDoPedro);
				
				System.out.println(listaPessoa.get(index).getProfissao());

		//
		// TRECHO ONDE VOC� PODE ESCREVER C�DIGO, SE QUISER. - FIM
		
				List<Pessoa> listaNaVariavel = obterListaPessoas();
				for(Pessoa pessoa : listaNaVariavel) {
					
				}
	}

	public static List<Pessoa> obterListaPessoas() {

		Pessoa pessoa01 = new Pessoa();
		pessoa01.setNome("Diogo");
		pessoa01.setIdade(27);
		pessoa01.setProfissao("Bombeiro");

		Pessoa pessoa02 = new Pessoa();
		pessoa02.setNome("Pedro");
		pessoa02.setIdade(27);
		pessoa02.setProfissao("Astronauta");

		Pessoa pessoa03 = new Pessoa();
		pessoa03.setNome("Jo�o");
		pessoa03.setIdade(27);
		pessoa03.setProfissao("Encanador");

		Pessoa pessoa04 = new Pessoa();
		pessoa04.setNome("Ana");
		pessoa04.setIdade(27);
		pessoa04.setProfissao("M�dica");

		List<Pessoa> listaPessoas = new ArrayList<Pessoa>();
		listaPessoas.add(pessoa01);
		listaPessoas.add(pessoa02);
		listaPessoas.add(pessoa03);
		listaPessoas.add(pessoa04);

		return listaPessoas;
	}

}
